// Import required modules.
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// create the Schema for job post
var loginSchema = new Schema({
    number: {
        type: String
    },
    otp: {
        type: String
    }
});

// we need to create a model for using schema
var logindata = mongoose.model('login_Schema', loginSchema);

// make this available to our jobs in our Node applications
module.exports = logindata;