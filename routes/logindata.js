//import the required modules
var express = require('express');
var router = express.Router();
var userController = require('../controllers/loginController');

router
//api for getting the otp while entering the mibile number
.get(
    '/v1/login/postMobileNumberForOtp/:number', userController.getMobileNumber
)
// api for validating the otp 
.get(
    '/v1/login/postOtpForAuthentication/:otp/:number', userController.validateOtp
)

//export the router
module.exports = router;