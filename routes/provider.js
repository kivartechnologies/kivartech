//import the required modules
var express = require('express');
var router = express.Router();
var userController = require('../controllers/ProviderController');

router
//api for create user (emplyee/employer)
.post(
    "/v1/provider/createUser", userController.postUser
)
//api for getting all user data
.get(
    '/v1/provider/getAllUser', userController.getAllUser
)
//api for getting all user data from his type(Emplyee/Employer)
.get(
    '/v1/provider/getAllUserType/:uType', userController.getAllUserType
)
//api for getting the user data from his id
.get(
    '/v1/provider/getUser/:id', userController.getUser
)
//api for updating the data of the user
.put(
    '/v1/provider/updateUser/:userId', userController.updateUser
)
//api for deleting the user
.delete(
    '/v1/provider/deleteUser/:userId', userController.deleteUser
)

//export the router
module.exports = router;